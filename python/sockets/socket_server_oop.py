import socket


class SocketServer:
    def __init__(self):
        self.node = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port_and_ip = ('127.0.0.1', 12345)
        self.node.bind(port_and_ip)
        self.node.listen(5)
        self.connection, addr = self.node.accept()

    def receiver(self):
        msg = ""
        while msg != "exit":
            msg = self.connection.recv(1024).decode()
            print(f"<<< {msg}")


if __name__ == '__main__':
    server = SocketServer()
    server.receiver()
