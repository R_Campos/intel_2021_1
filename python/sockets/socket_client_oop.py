import socket


class SocketClient:
    def __init__(self):
        self.node = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port_and_ip = ('127.0.0.1', 12345)
        self.node.connect(port_and_ip)

    def send_sms(self, SMS):
        self.node.send(SMS.encode())

    def sender(self):
        message = ""
        while message != "exit":
            message = input(">>> ")
            self.send_sms(message)


if __name__ == '__main__':
    client = SocketClient()
    client.sender()
