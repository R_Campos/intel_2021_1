# Python

* Fue creado por Guido Van Rossum

* Es un lenguaje multiparadigma

* Es un lenguaje interpretado

* Es Multiplataforma

## Áreas de aplicación

* AI

* Robótica

* Big data(área de aplicación mas fuerte)

* Ciencia

* Data sciencie

* Desarrollo de escritorio

* Aplicaciones web(Django y flask)

* machine learning

* scripting

## El Zen de Python

1. Lo bello es mejor que lo feo.
2. Explícito es mejor que implícito.
3. Mejor es simple que complejo.
4. Complejo es mejor que complicado.
5. Plano es mejor que anidado.
6. Es mejor escaso que denso.
7. La legibilidad cuenta.
8. Los casos especiales no son lo suficientemente especiales como para romper las reglas.
9. Aunque la practicidad vence a la pureza.
10. Los errores nunca deben pasar silenciosamente.
11. A menos que sea silenciado explícitamente.
12. Ante la ambigüedad, rechace la tentación de adivinar.
13. Debe haber una, y preferiblemente solo una, forma obvia de hacerlo.
14. Aunque esa forma puede no ser obvia al principio a menos que seas holandés.
15. Ahora es mejor que nunca.
16. Aunque nunca es mejor que * ahora mismo *.
17. Si la implementación es difícil de explicar, es una mala idea.
18. Si la implementación es fácil de explicar, puede ser una buena idea.
19. Los espacios de nombres son una gran idea, ¡hagamos más!

si necesitas recordar este zen solo importalo

````` python
import this
`````

## Hola mundo

````` python
print ("hola mundo")
`````
````` bash
python hola.py
`````
## Variables
````` python
# Integer
a = 2
print(a)
# Output: 2
# Integer
b = 9223372036854775807
print(b)
# Output: 9223372036854775807
# Floating point
pi = 3.14
print(pi)
# Output: 3.14
# String
c = 'A'
print(c)
# Output: A
# String
name = 'John Doe'
print(name)
# Output: John Doe
# Boolean
q = True
print(q)
# Output: True
# Empty value or null data type
x = None
print(x)
# Output: None
`````
### Ver tipo de variables
````` python
a = 2
print(type(a))
# Output: <type 'int'>
b = 9223372036854775807
print(type(b))
# Output: <type 'int'>
pi = 3.14
print(type(pi))
# Output: <type 'float'>
c = 'A'
print(type(c))
# Output: <type 'str'>
name = 'John Doe'
print(type(name))
# Output: <type 'str'>
q = True
print(type(q))
# Output: <type 'bool'>
7x = None
print(type(x))
# Output: <type 'NoneType'>
`````
### Asignar valores

* Asignación simple
  ````` python
  A="soy una vaariable"
  `````
* Asignar valores a multiples variables
  ````` python
  a, b, c = 1, 2, 3
  `````
## Iterables

### String Class
````` python
s = "hey its python"

# concatenacion
s += "and is amazing"
from datetime import datetime
date = datetime.now()
s = f"{s}{date}"
s.join((", but", " slow"))
s.zfill(100)

# cambiar formato
s.capitalize() # primera letra mayuscula
s.upper() # todas mayusculas
s.lower() # todas minusculas
s.swapcase() # cambia mayuscilas por minusculas y viseversa

# obtener caracteres por un rango de indices
s[2:5]
s.index('n') # indice de la primera aparicion
s.find("h") # buscar palabra dada
s.rfind("h") # buscar palabra pero iniciando desde el final
s.count("n") # contar las coincidencias
lista = s.split() # convertir a lista

# eliminar caracteres
s.strip("a") # eliminar letra
s.lstrip("h")# eliminar primera coincidencia
s.rstrip("o")# eliminar ultima coincidencia
s.replace("python", "julia")# reenplazar
`````

### Lista

````` python
Lista=[1,2,3,4]
lista2=[1,2.33,True,"hey"]
lissta3=[1,2,3,[4,5,6],7,8,9]
lista4=lista5=[1,2,3,4,5]
`````
#### Métodos

````` python
lista.append("tec")#apilar
lista.insert(1, "Cintalapa")#insertar(posición,valor)
del lista[2] #elimina un elemento en la posicion indicada
lista.remove(3)#elimina el elemento coincidente con el valor insetado
lista.clear() #vaciar lista
lista.pop()#elimina el ultimo elemento de la lista
lista.reverse()#invierte el orden
lista.count(1)#cueta la concurrencia de un valor
lista.extend(range(3))#agrega valores a una lista apartir de elementos iterables
lista.index(4)#indice de valores
lista.sort()#ordena elementos de me menor a mayor
lista1=list(range(10))#Crea una lista apartir de 0 a 9
from collections import Counter
Counter(lista) # contar la concurrencia de todos los valores
`````
#### Compresión

````` python
# Como regularmente lo hariamos
squares = []
for x in range(10):
    squares.append(x**2)

# una forma mas elegante
squares = list(map(lambda x: x**2, range(10)))

# forma pythonizada
squares = [x**2 for x in range(10)]
`````

### Tuplas
````` python
ip_address = ('10.20.30.40', 8080)
`````

### Diccionarios
````` python
state_capitals = {
'Arkansas': 'Little Rock',
'Colorado': 'Denver',
'California': 'Sacramento',
'Georgia': 'Atlanta'
}

state_capitals['Arkansas'] #accediendo a al valor de una llave

state_capitals.get('Arkansas') # una mejor manera de acceder a una llave la linea anterior de no exitir la lleve mandaria a una excepción por otro lado esta linea de no exitir la llave el valor seria "None"

state_capitals.get('Arkansas', 'Florida')#El segundo parametro indica el valor que tomara de no exitir la llave

state_capitals.keys() # listar todas la llaves disponibles

`````

### Sets
````` python
from functools import reduce
some_set = {"mac", "linux", "windows"}
some_set.add("bsd") # agregar
some_set.update(["unix", "sun", "haiku"]) # actualizar set con nuevos valores
some_set.remove("sun")
some_set.discard("unix")
some_set.pop()
other_set = {"oracleos", "reactos", "freebsd"}
some_set.union(other_set)
some_set.intersection({"linux", "mac"})
some_set.difference({"windows", "haiku"})
some_set.symmetric_difference({"linux", "windows"})
print("mac" in some_set)
print(set(filter(lambda x: len(x) > 4, some_set)))
print(reduce(lambda a, b: a + b, some_set))
some_set.clear()
`````

### Utils
````python
#  Encuentra la intersección de 2 listas
li1 = [1,2,3]
li2 = [2,3,4]
li3 = set (li1) & set (li2)
# => {2, 3}

# Encuentra la diferencia entre una lista y otra lista
conjunto (li1) - conjunto (li2)
# => {1}
conjunto (li2) - conjunto (li1)
# => {4}

# Suma el contenido de un iterable
sum([1, 2, 3])
# => 6

# Devuelve el contenido ordenado en un iterable
sorted("hola")
# => [a, h, l, o]

# Devuelve true si algun elemento es verdadero
any([False, False, True])
# => True

# Devuelve true si algun elemento es verdadero
all([False, False, True])
# => False
````

## Indentación
En python no existen "{}" para identificar bloques de código en python se utiliza algo denominado como indentación.
Ejemplo:

````` python
a,b=4,5
if a > b:
	print(a)
else:
	print(b)
`````

Compara con C#:
````` c#
using System;
class MainClass {
  public static void Main (string[] args) {
    int a=4,b=5;
    if(a>b){
      Console.WriteLine(a);
    }else{
      Console.WriteLine(b);
    }
  }
}
`````
## Condicionales

### if

````` python

# Condicional simple
a = 3
if a == 3:
    print(a)

# Condicional con opcion de escape
if a == 3:
    print(a)
else:
    print("el numero no es 3")

# Condicional con multiples opciones
if a == 3:
    print(a)
elif a == 2
    print("es un numero dos")
else:
    print("el numero no es 3")

`````
### switch
Para el creador de python esta funcion era redundante debido a que hay mejores
maneras de tener un condicional multiple que hacen un codigo mas limpio,
a continuacion se muestra una forma de hacerlo

````` python
nonswitch =  {
    1: "opcion 1",
    2: "opcion 2",
    3: "opcion 3",
}
opcion = 2
print(nonswitch[opcion])

`````

## Ciclos
````` python
for i in range(10):#ciclo for
   print(i)

i = 1
while i < 6:
  print(i)
  i += 1
i = 2

while i < 6:
  print(i)
  if i == 3:
    break#rompe el ciclo
  i += 1

i = 3
while i < 6:
  i += 1
  if i == 3:
    continue#el ciclo continua
  print(i)
`````
### extras del ciclo for:

````` python
some_list = ["hola", "soy", "una", "lista"]
for i in some_list:# iterar sobre un iterable
   print(i)

for index, value in enumerate(some_list):# retorna el indice y valor
   print(index, value)
other_list = ["hola", "soy", "otra", "lista"]

for i, j in zip(some_list, other_list): # itera sobre dos listas
    print(i, j)
`````

## Pedir datos
````` python
n=input("Dame un numero: ")
`````
````` python
n=int(input("Dame un numero"))
nombre=input("Dame un nombre")
nf=float(input("Dame un numero flotante"))
`````
