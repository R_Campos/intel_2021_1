import unittest
import contextlib
import io
from pathlib import Path
from protocolos import FactoryProtocol
from protocolos import Protocol
from protocolos import Http
from protocolos import Ftp
from protocolos import Smb


class TestProtocols(unittest.TestCase):
    def setUp(self):
        self.dir1 = 'test_dir1'
        self.dir2 = 'test_dir2'
        Path(self.dir1).mkdir(exist_ok=True)
        Path(self.dir2).mkdir(exist_ok=True)
        self.protocol = FactoryProtocol("dcrag@pm.me", "1234", "my-domain.com")
        self.spec_details = {
            "email": "dcrag@pm.me",
            "resource": "my-domain.com"
        }

    def test_subclass(self):
        self.assertFalse(issubclass(FactoryProtocol, Protocol))
        self.assertTrue(issubclass(Http, Protocol))
        self.assertTrue(issubclass(Ftp, Protocol))
        self.assertTrue(issubclass(Smb, Protocol))

    def test_http(self):
        http = self.protocol.build_connection("http")
        spec_connection = {
            "protocol": "http",
            "state": "Connection is successful",
            "url": "http://my-domain.com"
        }
        self.spec_details["port"] = 80
        self.assertEqual(http.connection(), spec_connection)
        self.assertEqual(http.get_response(), "<h1>http connection<h1>")
        self.assertEqual(http.get_details_connection("1234"), self.spec_details)
        self.assertEqual(http.get_details_connection("1233"), "Incorrect data")

    def test_ftp(self):
        ftp = self.protocol.build_connection("ftp")
        spec_connection = {
            "protocol": "ftp",
            "state": "Connection is successful",
            "url": "ftp://my-domain.com"
        }
        self.spec_details["port"] = 21
        self.assertEqual(ftp.connection(), spec_connection)
        list_dirs = ftp.get_response()
        self.assertTrue(self.dir1 in list_dirs)
        self.assertTrue(self.dir2 in list_dirs)
        self.assertTrue("protocolos.py" in list_dirs)
        self.assertEqual(ftp.get_details_connection("1234"), self.spec_details)
        self.assertEqual(ftp.get_details_connection("1233"), "Incorrect data")

    def test_smb(self):
        smb = self.protocol.build_connection("smb")
        spec_connection = {
            "protocol": "smb",
            "state": "Connection is successful",
            "url": "smb://my-domain.com"
        }
        self.spec_details["port"] = 445
        self.assertEqual(smb.connection(), spec_connection)
        list_dir_and_files = smb.get_response()
        self.assertTrue(self.dir1 in list_dir_and_files)
        self.assertTrue(self.dir2 in list_dir_and_files)
        self.assertFalse("protocolos.py" in list_dir_and_files)
        self.assertEqual(smb.get_details_connection("1234"), self.spec_details)
        self.assertEqual(smb.get_details_connection("1233"), "Incorrect data")

    def test_history(self):
        from time import localtime
        hour = localtime().tm_hour
        self.protocol.build_connection("http")
        self.protocol.build_connection("ftp")
        self.protocol.build_connection("ftp")
        self.protocol.build_connection("smb")
        history = self.protocol.get_history_connections()
        self.assertTrue(isinstance(history, list))
        self.assertTrue(f"smb {hour}" in history)
        self.assertTrue(f"http {hour}" in history)
        self.assertEqual(len(history), 4)
        self.assertEqual(history.count(f"ftp {hour}"), 2)

    def test_protocol_not_existis(self):
        self.assertEqual(
            self.protocol.build_connection("pop"),
            "The protocol does not exist in the current context")


if __name__ == '__main__':
    # find all tests in this module
    import __main__
    suite = unittest.TestLoader().loadTestsFromModule(__main__)
    with io.StringIO() as buf:
        # run the tests
        with contextlib.redirect_stdout(buf):
            unittest.TextTestRunner(stream=buf, verbosity=2).run(suite)
        # process (in this case: print) the results
        print(buf.getvalue())
