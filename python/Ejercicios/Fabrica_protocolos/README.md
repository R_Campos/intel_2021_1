# Fabrica de protocolos

Una aplicación necesita crear conexiones a un servidor por los siguientes
protocolos:

* `http`
* `ftp`
* `smb`


Crear una clase llamada Protocol que tenga los siguientes atributos :
* `email`
* `password`
* `port`
* `resource`

Cada protocolo(http, ftp, smb) debera heredar de Clase
Potocol teniendo cada una de ellas los siguientes métodos:

* **connection**() cuyo retorno es un diccionario. Tendrá la siguiente estructura:

``` python
    {
        "protocol": "http/ftp/smb",
        "state": "Connection is successful"
        "url": "genera la url correcpondiente"

    }
```

* **get_response**():
    * En smb retorna una lista de directorios correspondiente a la
        ruta actual donde se ejecuta el archivo .py
    * En ftp retorna la lista de directorios y archivos correcpondientes
        a donde se ejecuta el archivo .py
    * En http retorna la siguiente etiqueta \<h1\> http connection \<h1\>


* **get_details_connection**(password: str) retornar todos los atributos en
    forma de diccionario(toma en cuenta aspectos de seguridad) y si la
    contraseña es incorrecta retornar el mensaje "Incorrect data"



Para poder crear una nueva conexión a un protocolo se hara por medio de Clase
FactoryProtocol cuya clase no hereda de ninguna de las clases anteriores. Sus
atributos son(todos privados):

* `email`
* `password`
* `resource`

y tendra los siguientes metodos:

* **build_connection**(protocol: str) retorna una instancia de acuerdo al protocolo y si no existe retorna el mensaje "The protocol does not exist in the current context"

* **get_history_connections**() retorna una lista con todas las conexiones echas
    la estructura de cada elemento es la siguiente f'{protocolo} {hora de
    conexión(formato 24 horas)}


Ejemplo de implementación:
``` python
if __name__ == '__main__':
    protocol = FactoryProtocol("dcrag@pm.me", "1234", "my-domain.com")
    http_connection = protocol.build_connection("http") # Return istance of Http Class
    http_connection.connecion()  # Return:
    # {
    #   "protocol": "http",
    #   "state": "Connection is successful"
    #   "url": "http://my-domain.com"
    # }
    http_connection.get_response() # Return <h1>http connection<h1>
    http_connection.get_details_connection("1234")  # Return:
    # {
    #   "email": "dcrag@pm.me",
    #   "port": 80
    #   "resource": "my-domain.com"
    # }
    protocol.get_history_connections()  # Return ["http 14"] its format 24 hours
```

Nombre del archivo: protocolos.py

Leer atentamente las directrices y el enunciado.

Recuerda que si tienes dudas de los nombres de las clases puedes ver las
importaciones en el archivo test_protocolos.py
