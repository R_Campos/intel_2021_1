import sys
import csv
import subprocess
from pathlib import Path


def save_as_csv(csv_path: str, values: list[dict], fields: list):
    with open(csv_path, 'w') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fields)
        writer.writeheader()
        writer.writerows(values)


def csv_to_list_dict(csv_path: str):
    returned_list = list()
    file_csv = open(csv_path)
    values_dict = csv.DictReader(file_csv)
    for value in values_dict:
        returned_list.append(value)
    file_csv.close()
    return returned_list


def get_list_directorys():
    current_path = Path(".")
    return (d for d in current_path.iterdir() if d.is_dir())


def run_test(folder: str, pyfile_name: str, test_file: str):
    mv_folder = f"mv {folder}/{pyfile_name} {folder}/tests/"
    cmd_test = f"python {folder}/tests/{test_file} | tail -4 | xargs"
    result = subprocess.run(
        mv_folder + " && " + cmd_test,
        shell=True,
        stdout=subprocess.PIPE)
    results = result.stdout.decode("utf-8").split(" ")
    if len(results) == 7:
        err = results[6]
        return results[1], err[err.index("=") + 1: err.index(")")]
    else:
        return results[1], '0'


def write_rating(pyfile_name: str, test_file: str, csv_path: str, output_csv: str):
    fields = ['Nombre', 'TestT', 'TestE']
    students = csv_to_list_dict(csv_path)
    list_directorys = get_list_directorys()
    for folder in list_directorys:
        student_name = str(folder).replace('_', ' ')
        current_student = {
            fields[0]: student_name,
            fields[1]: '',
            fields[2]: ''
        }
        student_exists = students.count(current_student)
        if student_exists:
            total, erros = run_test(str(folder), pyfile_name, test_file)
            students.remove(current_student)
            current_student[fields[1]] = total
            current_student[fields[2]] = erros
            students.append(current_student)
    save_as_csv(output_csv, students, fields)


if __name__ == "__main__":
    write_rating(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4])
