import contextlib
import io
import unittest
from desempeno import desempeno


def float_range(start, end, step):
    assert (step != 0)
    if start < end:
        yield start
        yield from float_range(round(start + step, 1), end, step)


class TestDesempeno(unittest.TestCase):
    def test_desempeno_insufficient(self):
        for i in float_range(0, 6, 0.1):
            self.assertEqual("Insuficiente", desempeno(i))

    def test_desempeno_sufficient(self):
        for i in float_range(6, 7, 0.1):
            self.assertEqual("Suficiente", desempeno(i))

    def test_desempeno_good(self):
        for i in float_range(7, 8, 0.1):
            self.assertEqual("Bueno", desempeno(i))

    def test_desempeno_remarkable(self):
        for i in float_range(8, 9, 0.1):
            self.assertEqual("Notable", desempeno(i))

    def test_desempeno_excelent(self):
        for i in float_range(9, 10.1, 0.1):
            self.assertEqual("Excelente", desempeno(i))

    def test_desempeno_invalid_cal(self):
        self.assertEqual("Calificacion no valida", desempeno(-1))
        self.assertEqual("Calificacion no valida", desempeno(10.1))


if __name__ == '__main__':
    # find all tests in this module
    import __main__
    suite = unittest.TestLoader().loadTestsFromModule(__main__)
    with io.StringIO() as buf:
        # run the tests
        with contextlib.redirect_stdout(buf):
            unittest.TextTestRunner(stream=buf, verbosity=2).run(suite)
        # process (in this case: print) the results
        print(buf.getvalue())
