# Ejercicio de desempeño

Escriba una función que dado un promedio a su entrada retorne el nivel de desempeño, en caso de que el promedio no sea valido debera retornar el mensaje "Calificacion no valida"

| Nivel de desempeño | Promedio |
|--|--|
| Excelente | 9.0 a 10.0 |
| Notable | 8.0 a 8.9 |
| Bueno | 7.0 a 7.9 |
| Suficiente | 6.0 a 6.9 |
| Insuficiente | 0 a 5.9 |

Nombre del archivo: desempeno.py

Nombre de la función: desempeno

nota: Es importante que para este ejercicio uses la menor cantidad de if else,
hay mejores formas de resolverso sin recurrir a tantas sentencias if.

Leer las "directrices para la evaluación de ejercicios prácticos" en el README.md en la pagina principal del repositorio
