import unittest
import contextlib
import io
from poligonos import Poligono
from poligonos import Cuadrado
from poligonos import Triangulo
from poligonos import Pentagono


class TestListsInHistogram(unittest.TestCase):
    def test_cuadrado(self):
        cuadrado = Cuadrado(4)
        self.assertTrue(isinstance(cuadrado, Poligono))
        self.assertEqual(cuadrado.area(), 16)
        self.assertEqual(cuadrado.perimetro(), 16)

    def test_triangulo(self):
        triangulo = Triangulo(4)
        self.assertTrue(isinstance(triangulo, Poligono))
        self.assertEqual(round(triangulo.area(), 1), 6.9)
        self.assertEqual(triangulo.perimetro(), 12)

    def test_pentagono(self):
        pentagono = Pentagono(4)
        self.assertTrue(isinstance(pentagono, Poligono))
        self.assertEqual(round(pentagono.area(), 2), 27.53)
        self.assertEqual(pentagono.perimetro(), 20)


if __name__ == '__main__':
    # find all tests in this module
    import __main__
    suite = unittest.TestLoader().loadTestsFromModule(__main__)
    with io.StringIO() as buf:
        # run the tests
        with contextlib.redirect_stdout(buf):
            unittest.TextTestRunner(stream=buf, verbosity=2).run(suite)
        # process (in this case: print) the results
        print(buf.getvalue())
