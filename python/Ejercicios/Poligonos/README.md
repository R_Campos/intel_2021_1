# Ejercicio de Herencia

Realizar una clase llamada Poligono que tenga los atributos: base, altura y lados. Además de los métodos: perimetro y area(deben retornar el resultado correspondiente). Realizar 3 subclases llamadas Cuadrado, Triangulo y Pentagono, que hereden las características de la clase superior y que cada objeto instanciado calcule su área de manera adecuada con el método area. Comprobar que las áreas y perímetros son correctamente calculados.

Especificaciones:

* Cada subclase solo recibe un atributo llamado lado

Nombre del archivo: poligonos.py

nota: La clase Triangulo hace referencia aun triangulo equilatero

Leer las "directrices para la evaluación de ejercicios prácticos" en el README.md en la pagina principal del repositorio
