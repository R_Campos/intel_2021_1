from histograma import get_histogram
import unittest
import contextlib
import io


def generate_repeat_number_list(seed: int, repetitions: int):
    return [seed] * repetitions


class TestListsInHistogram(unittest.TestCase):
    def test_histogram(self):
        list_1 = [3, 2, 1]
        list_2 = [11, 14, 23]
        for i in range(4, 10):
            list_1.extend(generate_repeat_number_list(i, i + 3))

        for i in range(3, 7):
            list_2.extend(generate_repeat_number_list(i, i - 2))

        self.assertEqual(
            get_histogram(list_1),
            {1: 1, 2: 1, 3: 1, 4: 7, 5: 8, 6: 9, 7: 10, 8: 11, 9: 12})

        self.assertEqual(
            get_histogram(list_2),
            {11: 1, 14: 1, 23: 1, 3: 1, 4: 2, 5: 3, 6: 4})


if __name__ == '__main__':
    # find all tests in this module
    import __main__
    suite = unittest.TestLoader().loadTestsFromModule(__main__)
    with io.StringIO() as buf:
        # run the tests
        with contextlib.redirect_stdout(buf):
            unittest.TextTestRunner(stream=buf, verbosity=2).run(suite)
        # process (in this case: print) the results
        print(buf.getvalue())
