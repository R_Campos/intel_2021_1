# Ejercicios

Todos los ejercicios contaran con una carpeta de pruebas unitarias, la cual debe
incluirse en el archivo de entrega(leer las directrices para la entrega) este
test no debe ser modificado por el alumno(puede hacerlo pero para la entrega
debe venir en su estado original)

# Importante
* Incluir el archivo requirements.txt(si usas librerias externas)
* Incluir Reporte y diagramas (no se entrega encaso de que explicitamente se
    diga que no se requiere)
* Nombrado correcto de carpeta principal(se explica en las directrices)

## Uso del test unitario

En una terminal escribe lo siguiente

``` bash
python3 -m unittest discover tests

```
