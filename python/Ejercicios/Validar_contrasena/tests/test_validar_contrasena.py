import contextlib
import io
import unittest
from contrasena import validar_contrasena
from random import randint, choice
import string


def generate_ramdom_string():
    chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
    chars_rules = ["b", "k"]
    ramdom_string = ""
    while len(ramdom_string) <= 7:
        char = choice(chars)
        for cr in chars_rules:
            if cr == char:
                char = ""
        ramdom_string += char
    return ramdom_string


def generate_correct_password(chars_rules=["b", "b", "k", "*+%"]):
    i = 0
    lpos = []
    cp_list = list(generate_ramdom_string())
    num_cr = len(chars_rules) - 1
    while i <= num_cr:
        pos = randint(0, 7)
        if pos in lpos:
            continue
        if i == num_cr:
            cp_list[pos] = chars_rules[num_cr][randint(0, 2)]
        else:
            cp_list[pos] = chars_rules[i]
        lpos.append(pos)
        i += 1
    return "".join(cp_list)


class TestValidarContrasena(unittest.TestCase):
    def test_valid_password(self):
        correct_passwords = [
            generate_correct_password()
            for _ in range(5)
        ] + [
            generate_correct_password(chars_rules=["b", "b", "b", "k", "*+%"])
            for _ in range(5)]

        for i, cp in enumerate(correct_passwords):
            self.assertTrue(validar_contrasena(cp))

    def test_invalid_password(self):
        incorrect_passwords = [
            generate_ramdom_string()
            for _ in range(5)
        ] + [
                generate_correct_password(chars_rules=["b", "k", "*+%"])
                for _ in range(5)]
        for i, ip in enumerate(incorrect_passwords):
            self.assertFalse(validar_contrasena(ip))

    def test_long_password(self):
        self.assertFalse(validar_contrasena("bkb*34yt0"))

    def test_short_password(self):
        self.assertFalse(validar_contrasena("bkb*34y"))


if __name__ == '__main__':
    # find all tests in this module
    import __main__
    suite = unittest.TestLoader().loadTestsFromModule(__main__)
    with io.StringIO() as buf:
        # run the tests
        with contextlib.redirect_stdout(buf):
            unittest.TextTestRunner(stream=buf, verbosity=2).run(suite)
        # process (in this case: print) the results
        print(buf.getvalue())
