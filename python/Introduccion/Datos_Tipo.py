# Conocer el tipo de dato
num = 100
print("type of num: ", type(num))
print(num)
del num  # Eliminar un elemento

# float
num2 = 10.99
print("type of num2: ", type(num2))

# número complejo
num3 = 3 + 4j
print("type of num3: ", type(num3))

