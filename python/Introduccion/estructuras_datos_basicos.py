# --------------Listas--------------------
lista = [1, 2, 3, 4]
lista2 = [1, 2.33, True, "hey"]  # admite tipos mixtos
lista3 = [1, 2, 3, [4, 5, 6], 7, 8, 9]  # es posible tener una lista de listas
lista4 = lista5 = [1, 2, 3, 4, 5]

lista2[2] = False  # cambiar un valor por indice

print("imprimiendo las listas")
print(lista)
print(lista2)
print(lista3)
print(lista4)
print(lista5)

# Métodos
lista.append("tec")  # apilar
print('Apilando el valor "tec" a la lista')
print(lista)
lista.insert(1, 16)  # insertar(posición,valor)
print('Insertando el valor 16 en la posicion 1 a la lista')
print(lista)
del lista[2]  # elimina un elemento en la posicion indicada
print('Eliminando el elmento en la posicion 2')
print(lista)
lista.remove(3)  # elimina el elemento coincidente con el valor insetado
print('Eliminando el valor 3')
print(lista)
print('Eliminando el ultimo valor de la lista')
lista.pop()  # elimina el ultimo elemento de la lista
print(lista)
lista.reverse()  # invierte el orden
print('invirtiendo el orden de la lista')
print(lista)
lista.count(1)  # cueta la concurrencia de un valor
print('Contando la concurrencia del valor 1')
print(lista)
lista.extend(range(3))  # agrega valores a una lista apartir de elementos iterables
print('Extendiendo la lista con un rango de 0 a 3')
print(lista)
lista.index(4)  # indice de valores
print('Obtener el indice del valor 4')
print(lista)
lista.sort()  # ordena elementos de me menor a mayor
print('Ordenar los elementos de mayor a menor')
print(lista)
lista.clear()  # vaciar lista
print('Vaciar la lista')
lista1 = list(range(10))  # Crea una lista apartir de 0 a 9
print(lista)
print('Nueva lista creada con un rango')
print(lista1)

# Compresión

# Como regularmente lo hariamos
squares = []
for x in range(10):
    squares.append(x**2)

# con compresión
print('Nueva lista de elementos creada a la potencia 2')
squares = [x**2 for x in range(10)]

# Encontrando valores
print(f"¿El 9 exite en la lista?\n {9 in squares}")

# ------------------------------------------------

# -----------------Tuplas------------------------
ip_address = ('10.20.30.40', 8080)
# Descomenta para ver los errores
# ip_address.insert(0, '10.20.30.50')
# del ip_address[1]
# ip_address[1] = 80

# -----------------------------------------------

# ------------------Diccionarios-----------------
print('Diccionarios')
state_capitals = {
    'Arkansas': 'Little Rock',
    'Colorado': 'Denver',
    'California': 'Sacramento',
    'Georgia': 'Atlanta'
}

print('Accediendo a un valor por clave')
print(state_capitals['Arkansas'])  # accediendo a al valor de una llave

print('Accediendo a un valor por clave manejando nulos')
print(state_capitals.get('Arkansas'))  # una mejor manera de acceder a una llave la linea anterior de no exitir la lleve mandaria a una excepción por otro lado esta linea de no exitir la llave el valor seria "None"

print('Accediendo a un valor por clave de no existir lanza la opcion 2')
print(state_capitals.get('DC', 'Washington'))  # El segundo parametro indica el valor que tomara de no exitir la llave

# Agregar un nuevo elemento
print('Agregando un nuevo elemento')
state_capitals["DC"] = "Washington"

# Unir o actualizar elementos < python 3.9
print('Unir o actualizar elementos')
state_capitals.update({"Texas": "Austin"})

# Unit o actualizar elementos > python 3.9
state_capitals | {"Texas": "Austin"}

print('Listar todas la llaves')
print(state_capitals.keys())  # listar todas la llaves disponibles
print('Listar todos los valores')
print(state_capitals.values())  # listar todos los valores disponibles
print('Listar todos los elementos como una lista')
print(state_capitals.items())  # Comvetir a filas un diccionario [(clave, valor),...]
# -----------------------------------------------

# -------------Conjuntos------------------------
print('Conjuntos')
conjunto = {1, 2, 3, 4}
conjunto2 = {2, 4, 6, 8}

# Agregar
print('Agregar conjuntos')
conjunto.add(5)
print(conjunto)
print('actualizar conjuntos')
conjunto.update({6, 7})  # Acepta cualquier iterable
print(conjunto)

# Eliminar
print('eliminar elemento del conjunto')
conjunto.remove(7)
print(conjunto)

# Union
print('Union')
print(conjunto | conjunto2)

# Interseccion
print('Interseccion')
print(conjunto & conjunto2)

# Diferencia
print('Diferencia')
print(conjunto - conjunto2)

# es posible hacer esto con funciones incluidas en la clase Set

# ----------------------------------------------
