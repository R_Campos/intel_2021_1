#Eliminar elementos de una lista
# lista de numeros
n_list = [1, 2, 3, 4, 5, 6]

# Eliminar el segundo elemento
del n_list[1]

# lista: [1, 3, 4, 5, 6]
print(n_list)

# Eliminar los elementos del segundo al cuarto
del n_list[2:4]

# lista: [1, 3, 6]
print(n_list)

# Eliminando la lista completa
del n_list