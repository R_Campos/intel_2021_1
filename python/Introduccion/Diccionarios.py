# Creando y accediendo a datos denteo de un diccionario
print("Ejemplo 1")
mydict = {'StuName': 'Ajeet', 'StuAge': 30, 'StuCity': 'Agra'}
print("Student Age is:", mydict['StuAge'])
print("Student City is:", mydict['StuCity'])

# Modificando valores de un diccionario
print("Ejemplo 2")
mydict = {'StuName': 'Ajeet', 'StuAge': 30, 'StuCity': 'Agra'}
print("Student Age before update is:", mydict['StuAge'])
print("Student City before update is:", mydict['StuCity'])
mydict['StuAge'] = 31
mydict['StuCity'] = 'Noida'
print("Student Age after update is:", mydict['StuAge'])
print("Student City after update is:", mydict['StuCity'])

# Añadiendo campos al diccionario
print("Ejemplo 3")
mydict = {'StuName': 'Steve', 'StuAge': 4, 'StuCity': 'Agra'}
mydict['StuClass'] = 'Jr.KG'
print("Student Name is:", mydict['StuName'])
print("Student Class is:", mydict['StuClass'])


# Iteracion sobre diccionarios
print("Ejemplo 4")
mydict = {'StuName': 'Steve', 'StuAge': 4, 'StuCity': 'Agra'}
for e in mydict:
    print("Key:", e, "Value:", mydict[e])

# Eliminando elemento en diccionario
print("Ejemplo 5")
ydict = {'StuName': 'Steve', 'StuAge': 4, 'StuCity': 'Agra'}
del mydict['StuCity']  # remover una clave
mydict.clear()  # remover todas las clave-valor del diccionario
del mydict  # eliminar el diccionario completo

# Otra sintaxis
print("Ejemplo 6")
thisdict = {
  "brand": "Ford",
  "model": "Mustang",
  "year": 1964
}
print(thisdict)
# Leer valor de clave con funcion get()
x = thisdict.get("model")
print(x)
