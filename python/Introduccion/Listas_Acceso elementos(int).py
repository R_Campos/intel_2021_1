# Acceso a elementos de una lista
# una lista de números
numbers = [11, 22, 33, 100, 200, 300]

# print 11
print(numbers[0])

# print 300
print(numbers[5])

# print 22
print(numbers[1])

# error
print(numbers[1.0])