Set Methods

1. add(): This method adds an element to the Set.
2. remove(): This method removes a specified element from the Set
3. discard(): This method works same as remove() method, however it doesn’t raise an error when the specified element doesn’t exist.
4. clear(): Removes all the elements from the set.
5. copy(): Returns a shallow copy of the set.
6. difference(): This method returns a new set which is a difference between two given sets.
7. difference_update(): Updates the calling set with the Set difference of two given sets.
8. intersection(): Returns a new set which contains the elements that are common to all the sets.
9. intersection_update(): Updates the calling set with the Set intersection of two given sets.
10. isdisjoint(): Checks whether two sets are disjoint or not. Two sets are disjoint if they have no common elements.
11. issubset(): Checks whether a set is a subset of another given set.
12. pop(): Removes and returns a random element from the set.
13. union(): Returns a new set with the distinct elements of all the sets.
14. update(): Adds elements to a set from other passed iterable.
15. symmetric_difference(): Returns a new set which is a symmetric difference of two given sets.
16. symmetric_difference_update(): Updates the calling set with the symmetric difference of two given sets.