# La mejor forma de enentender los generadores es con un ejemplo
# analiza el siguiente ejemplo

def reverse(data: str):
    for index in range(len(data)-1, -1, -1):
        yield data[index]


nohtyp = reverse("Python")
print(nohtyp)
print(next(nohtyp))
print(next(nohtyp))
print(next(nohtyp))
print("--- Las letras que fantan ---")
for i in nohtyp:
    print(i)

# Los generadores son eficientes en memoria
