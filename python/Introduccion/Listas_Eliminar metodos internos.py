# Eliminar elementos de una lista utilizando metodos internos
# lista de caracteres
ch_list = ['A', 'F', 'B', 'Z', 'O', 'L', 'B']

# Eliminando el dato con el valor B
ch_list.remove('B')

# lista: ['A', 'F', 'Z', 'O', 'L']
print(ch_list)

# Eliminando el dato con el valor B
ch_list.remove('B')

# lista: ['A', 'F', 'Z', 'O', 'L']
print(ch_list)

# Eliminando el segundo elemento
ch_list.pop(1)

# lista: ['A', 'Z', 'O', 'L']
print(ch_list)

# Eliminando todos los elementos
ch_list.clear()

# list: []
print(ch_list)

