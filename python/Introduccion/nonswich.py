def fun1(a):
    return a


def fun2(b):
    return b


def fun3(*args):
    return "una funcion con *args"


def fundefault(*args):
    return "una funcion default"


nons = {
    1: fun1,
    2: fun2,
    3: fun3
}

opcion = int(input("selecciona una opcion: "))

print(nons.get(opcion, fundefault)())
