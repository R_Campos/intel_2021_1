# Acceso a elementos de una lista
# una lista de strings
my_list = ["hello", "world", "hi", "bye"]

# print "bye"
print(my_list[-1])

# print "world"
print(my_list[-3])

# print "hello"
print(my_list[-4])