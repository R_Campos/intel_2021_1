# Conjunto sin orden ni indexación
print("Ejemplo 1")
myset = {"hi", 2, "bye", "Hello World"}
print(myset)

# Iteracion sobre los elementos del set
print("Ejemplo 2")
myset = {"hi", 2, "bye", "Hello World"}
for a in myset:
    print(a)

# Añadir y eliminar un elemento del set
print("Ejemplo 3")
myset = {"hi", 2, "bye", "Hello World"}
print("Conjunto original:", myset)
# añadir un elemento
myset.add(99)
print("Conjunto después de añadir 99:", myset)
# eliminar un elemento
myset.remove("bye")
print("Conjunto despues de remover bye:", myset)
