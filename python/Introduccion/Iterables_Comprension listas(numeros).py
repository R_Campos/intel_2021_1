print("Ejemplo 1:")
# Crear una lista con las potencias de 2 de los primeros 10 números:
# Método tradicional
lista = []
for numero in range(0, 11):
    lista.append(numero**2)
print(lista)

# Con comprensión de listas
lista = []
lista = [numero**2 for numero in range(0, 11)]
print(lista)

print("Ejemplo 2:")
# Crear una lista con los todos los múltiples de 2 entre 0 y 10:
# Método tradicional
lista = []
for numero in range(0, 11):
    if numero % 2 == 0:
        lista.append(numero)
print(lista)

# Con comprensión de listas
lista = []
lista = [numero for numero in range(0, 11) if numero % 2 == 0]
print(lista)


print("Ejemplo 3:")
# Crear una lista de pares a partir de otra lista creada con las potencias de 2 de los primeros 10 números:
# Método tradicional
lista = []
for numero in range(0, 11):
    lista.append(numero**2)

pares = []
for numero in lista:
    if numero % 2 == 0:
        pares.append(numero)

print(pares)

# Con comprensión de listas
lista = [
    numero for numero in
    [numero**2 for numero in range(0, 11)]
    if numero % 2 == 0]
print(lista)

