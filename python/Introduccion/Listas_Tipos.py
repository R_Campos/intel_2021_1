# Tipos de listas
# lista de flotantes
num_list = [11.22, 9.9, 78.34, 12.0]

# lista de ebteros flotante y strings
mix_list = [1.13, 2, 5, "beginnersbook", 100, "hi"]

# una lista vacía
nodata_list = []

print(num_list)

print(mix_list)

print(nodata_list)