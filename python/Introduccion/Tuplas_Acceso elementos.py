# tuple of strings
my_data = ("hi", "hello", "bye")

# displaying all elements
print(my_data)

# accessing first element
# prints "hi"
print(my_data[0])

# accessing third element
# prints "bye"
print(my_data[2])


my_data2 = (1, 2, "Kevin", 8.9)

# accessing last element
# prints 8.9
print(my_data2[-1])

# prints 2
print(my_data2[-3])