# Añadir elementos a lista
# lista de números
n_list = [1, 2, 3, 4, 5, 6, 7]

# listar los elementos del primero al tercero
print(n_list[1:3])

# listar los elementos hasta del tercero
print(n_list[:3])

# listar los elementos a partir del tercero
print(n_list[3:])

# lista completa
print(n_list[:])

