# Crear una lista con las letras de una palabra:
# Método tradicional
lista = []
for letra in 'casa':
    lista.append(letra)
print(lista)

# Con comprensión de listas
# La lista está formada por cada letra que recorremos en el for
lista = [letra for letra in 'casa']
print(lista)

