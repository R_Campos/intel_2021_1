from psutil import process_iter as procsiter
from os import system as sy

if "spotify" in (p.name() for p in procsiter()):
    sy("spotifycli --playpause")
else:
    sy("env LD_PRELOAD=/usr/lib/spotify-adblock.so spotify %U")
