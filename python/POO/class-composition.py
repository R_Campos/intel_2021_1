class Salary:
    def __init__(self, pay):
        self.pay = pay

    def get_total(self):
        return (self.pay * 12)

    def tabulador(self):
        return 100


class Employee:
    def __init__(self, pay, bonus):
        self.pay = pay
        self.bonus = bonus
        self.obj_salary = Salary(self.pay)

    def annual_salary(self):
        return "Total: " + str(self.obj_salary.get_total() + self.bonus)

    def __getattr__(self, attr):
        return getattr(self.obj_salary, attr)


obj_emp = Employee(600, 500)
print(obj_emp.annual_salary())
print(obj_emp.tabulador())
