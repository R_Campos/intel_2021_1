from ecies.utils import generate_eth_key
from ecies import encrypt, decrypt
from base64 import b85encode, b85decode
import logging


logging.basicConfig(level=logging.DEBUG, format="\n%(asctime)s - %(message)s")


class Ecc:
    """Elliptic Curve Integrated Encryption Scheme"""

    def __init__(self):
        self.__private_key = generate_eth_key()
        self.__private_key_hex = self.__private_key.to_hex()
        print(self.__private_key_hex)

    @property
    def public_key(self) -> str:
        return self.__private_key.public_key.to_hex()

    def __base85_decode(self, message: str) -> bytes:
        message_to_bytes = message.encode("utf-8")
        b85_decode_message = b85decode(message_to_bytes)
        return b85_decode_message

    def decipher(self, encrypted_message: str) -> str:
        encrypted_msg_decode = self.__base85_decode(encrypted_message)
        decrypted_message = decrypt(
            self.__private_key_hex, encrypted_msg_decode
        )
        decrypted_message_string = decrypted_message.decode("utf-8")
        return decrypted_message_string

    @staticmethod
    def cypher(plain_message: str, publickey: str) -> str:
        plain_message_to_bytes = plain_message.encode("utf-8")
        cypher_message = encrypt(publickey, plain_message_to_bytes)
        b85_cypher = b85encode(cypher_message).decode("utf-8")
        return b85_cypher


logging.debug("Obteniendo la llave publica")
ecc_cypher = Ecc()
pubKey = ecc_cypher.public_key
print("Public Key:", pubKey)

msg = "any message to encrypt"

logging.debug("Cifrando el mensaje")
print("Plaintext:", msg)
encrypted = ecc_cypher.cypher(msg, pubKey)
print("Encrypted message")
print("Encrypted:", encrypted)

logging.debug("Decifrando el message cifrado")
decrypted = ecc_cypher.decipher(encrypted)
print("Decrypted:", decrypted)
