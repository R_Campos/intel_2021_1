class Room:
    def __init__(self, tenant: object = None, **kwargs):
        self.size = kwargs.get("size", [10, 15])
        self.color = kwargs.get("color", "Green")
        self.is_furnished = kwargs.get("furnished", True)
        self.__tenant = tenant

    def its_busy_room(self):
        return "Yes" if self.__tenant else "No"

    def __str__(self):
        return f"size: {self.size}\ncolor: {self.color}\nis_furnished: {self.is_furnished}"


class Person:
    def __init__(self, first_name, last_name):
        self.first_name = first_name
        self.last_name = last_name


Diego = Person("Diego", "Crag")
my_room = Room(Diego, size=[30, 30])

print(my_room.its_busy_room())
print(my_room)
