class Corral:
    def __init__(self, especies):
        self.__especies = especies
        self.__especie = "No hay especie"

    @property
    def especie(self):
        """ getter """
        return self.__especie

    @especie.setter
    def especie(self, especie):
        """ setter """
        if especie in self.__especies:
            self.__especie = especie
        else:
            raise ValueError(f'El corral no esta echo para la especie {especie}')


if __name__ == "__main__":
    my_corral = Corral(['Equina', 'Bovina', 'Caprina'])
    print(my_corral.especie)
    my_corral.especie = 'Equina'
    print(my_corral.especie)
