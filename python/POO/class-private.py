class Privado:
    def __init__(self):  # Constructor
        self.__attributo_privado = "Privado"  # atributo privado
        self.__metodo_privado()  # Si puede ser llamando

    def __metodo_privado(self):  # Metodo Privado
        print(f'Metodo {self.__attributo_privado}')


privado = Privado()

# print(privado.__attributo_privado)

# privado.__metodo_privado()
