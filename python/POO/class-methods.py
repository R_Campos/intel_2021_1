class Methods:
    arg1 = 3

    def __init__(self, arg1: int):
        self.arg1 = arg1

    def instance_method(self):
        print("access to instance attributes", self.arg1)

    @classmethod
    def class_method(cls):
        print("access to class attributes", cls.arg1)

    @staticmethod
    def static_method(a: int, b: int):
        print("only access to args of method", a, b)


methods_example = Methods(7)

methods_example.instance_method()  # 7
methods_example.class_method()  # 3
methods_example.static_method(3, 4)
