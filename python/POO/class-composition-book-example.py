class Book:
    def __init__(self):
        page1 = Page("This is content for page 1")
        page2 = Page("This is content for page 2")
        self.pages = [str(page1), str(page2)]


class Page:
    def __init__(self, content: str):
        self.__content = content

    def __str__(self):
        return self.__content


new_book = Book()
print(new_book.pages)
