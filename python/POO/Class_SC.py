import hashlib
import os
import base64

from Crypto.Cipher import AES


class Security:
    IV_SIZE = 16  # 128 bit, fixed for the AES algorithm
    KEY_SIZE = 32  # 256 bit meaning AES-256, can also be 128 or 192 bits
    SALT_SIZE = 16  # This size is arbitrary

    def __init__(self):
        password = b"highly secure encryption password"
        self.salt = os.urandom(self.SALT_SIZE)
        self.derived = hashlib.pbkdf2_hmac(
            "sha256",
            password,
            self.salt,
            100000,
            dklen=self.IV_SIZE + self.KEY_SIZE,
        )
        self.iv = self.derived[0 : self.IV_SIZE]
        self.key = self.derived[self.IV_SIZE :]

    def get_aes_obj(self):
        return AES.new(self.key, AES.MODE_CFB, self.iv)

    def encrypt(self, msg):
        msg = msg.encode("utf-8")
        b64_encode = base64.b64encode(
            self.salt + self.get_aes_obj().encrypt(msg)
        )
        return b64_encode.decode("utf-8")

    def decrypt(self, hash):
        b64_decode = base64.b64decode(hash.encode("utf-8"))
        plain_text = self.get_aes_obj().decrypt(b64_decode[self.SALT_SIZE :])
        return plain_text.decode("utf-8")


security = Security()
encrypted = security.encrypt("hola mundo")
print(encrypted)
print(security.decrypt(encrypted))
