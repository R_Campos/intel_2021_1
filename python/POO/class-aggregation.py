class Salary:
    def __init__(self, pay):
        self.pay = pay

    def get_total(self):
        return self.pay * 12

    def tabulador(self):
        return 100


class Employee:
    def __init__(self, obj_salary, bonus):
        self.bonus = bonus
        self.__obj_salary = obj_salary

    def annual_salary(self):
        return "Total: " + str(self.__obj_salary.get_total() + self.bonus)

    def __getattr__(self, attr):
        return getattr(self.__obj_salary, attr)


obj_salary = Salary(600)
obj_emp = Employee(obj_salary, 500)
obj_emp2 = Employee(obj_salary, 100)
print(obj_emp2.annual_salary())
print(obj_emp.annual_salary())
print(obj_emp.tabulador())
