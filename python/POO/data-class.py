from dataclasses import dataclass

@dataclass
class Card:
    rank: str
    suit: str

card = Card("Q", "hearts")
card2 = Card(rank='Q', suit='hearts')

print(card == card2)
# True

print(card.rank)
# 'Q'

print(card)
