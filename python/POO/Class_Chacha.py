import os
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.ciphers.aead import ChaCha20Poly1305

key = b"key must be 128, 196 or 256 bits"


class Secrets:
    def __init__(self):
        self.iv = os.urandom(16)

    def encrypt(self, data):
        cipher = Cipher(
            algorithms.AES(key), modes.CBC(self.iv), backend=default_backend()
        )
        encryptor = cipher.encryptor()
        return encryptor.update(data) + encryptor.finalize()

    def decrypt(self, data):
        #  iv = secrets.token_bytes(16)
        cipher = Cipher(
            algorithms.AES(key), modes.CBC(self.iv), backend=default_backend()
        )
        decryptor = cipher.decryptor()
        return decryptor.update(data) + decryptor.finalize()


#  my_secret = Secrets()
#  plaintext = b'la patita fue al mercado'
#  x = my_secret.encrypt(plaintext)
#  print(my_secret.decrypt(x))
data = b"a secret message"
aad = b"authenticated but unencrypted data"
#  key = ChaCha20Poly1305.generate_key()
chacha = ChaCha20Poly1305(key)
nonce = os.urandom(12)
ct = chacha.encrypt(nonce, data, aad)
print(ct)
print(chacha.decrypt(nonce, ct, aad))
