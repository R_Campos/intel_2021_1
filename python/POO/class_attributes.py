import logging

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(message)s'
)


class Pointers:
    x = 10
    y = 15
    _x = 10  # * protegido
    _y = 15
    __x = 10  # * privado
    __y = 15


point = Pointers()

logging.debug("imprimiendo valibles publicas y protegidas")
print(point.x)
# print(point._x)
# print(point.__x)
