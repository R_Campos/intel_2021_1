# Python y linux 1 - Exámen 1
## Instrucciones:

Dado un arreglo de tamaño [n, n], escribir una función llamada "ordenamiento" que reciba dicho arreglo y retorne un arreglo con los elementos ordenados desde la orilla hasta el centro en sentido horario.



arreglo = [[1,2,3],
          			[4,5,6],
          			[7,8,9]]

ordenamiento(arreglo) #=> [1,2,3,6,9,8,7,4,5]



A continuación se muestra un ejemplo del recorrido:



![ejemplo de ordenamiento](ejemplo_recorrido.jpg)



Se deberá entregar un breve reporte con diagrama de flujo de la solución propuesta, además del
código de la implementación.

Nota: Un arreglo vacío se escribe de la forma [[]]; si se entrega un arreglo vacio se retorna []

Nombre del archivo: examen1.py

Nombre de la función: ordenamiento
