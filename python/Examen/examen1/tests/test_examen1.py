import unittest
import contextlib
import io
from examen1 import ordenamiento


def generate_random_2d_lists(width: int):
    from random import randint

    height = width
    tiles = list(range(1000))
    list_2d = []
    for x in range(height):
        list_2d.append([])
        for _ in range(width):
            list_2d[x].append(tiles[randint(0, len(tiles) - 1)])
    return list_2d, width


def el(a):
    if not a:
        return []
    return list(tuple(a[0]) + tuple(el(list(zip(*a[1:]))[::-1])))


class Testordenar(unittest.TestCase):
    def test_valid_order(self):
        # print("Matrices a evaluar")
        for i in range(1, 150):
            ea, size = generate_random_2d_lists(i)
            # print(f"\nMatriz {i + 1}: {size} x {size}")
            # print(ea)
            self.assertEqual(el(ea), ordenamiento(ea))

    def test_array_empty(self):
        self.assertEqual([], ordenamiento([[]]))


if __name__ == "__main__":
    # find all tests in this module
    import __main__

    suite = unittest.TestLoader().loadTestsFromModule(__main__)
    with io.StringIO() as buf:
        # run the tests
        with contextlib.redirect_stdout(buf):
            unittest.TextTestRunner(stream=buf).run(suite)
        # process (in this case: print) the results
        print("%s" % buf.getvalue())
