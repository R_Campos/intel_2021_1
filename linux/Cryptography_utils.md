# base32
Codifica y decodifica en base32

# base64
Codifica y decodifica en base64

# Comprobar la integridad de los datos
¿Quien te asegura que un archivo es o contiene lo que dice ser?
Afortunadamente hay mecanismos matematicos que te permiten verificar la integridad de los archivo, y en el caso de linux como otros SO ya existen abstrabciones de estos procesos en forma de comandos


## md5(128 bits output)

Calculamos el hash con md3

``` bash
md5sum file
```

##  sha(160 bits to 512 bits output)

Calculamos el hash con sha

``` bash
sha<bits> file
```
## Comprobar la integridad con commandos linux
La estructura es muy sencilla y dependera de la forma es la que se te es
entregado el hash de integridad

Por ejemplo si tenemos un archivo con la integridad del archivo a evaluar

`<sum_command> -c SUMS 2> /dev/zero | grep -v FAILED`

cambia `sum_command` por cualquiera e los comandos anteriores

Por cierto respecto a este parte:

`2> /dev/zero | grep -v FAILED`

Es para evitar que salgan en terminal unos cuantos errores ya que los ficheros de checksum contienen las sumas de verificación de TODOS los ficheros e intentará verificarlos(en caso que se referenceen mas archivos). Al no haber descargado todos esos ficheros, nos encontraremos con largos errores de tipo:

Que evidentemente hacen referencia a los ficheros que no ha podido localizar para realizar las comprobaciones pertinentes. De esa manera tendremos una salida de resultados más limpia.

Si tenemos solo la salida proporcionado por el comando que calcula el hash:

`echo "<hash>  <filename>" | <sum_command> -c`

cambia `hash` por el hash del archivo a verificar y cambia `filename` por el
nombre del archivo a verificar

# openssl

Consiste en un robusto paquete de herramientas de administración y bibliotecas relacionadas con la criptografía, que suministran funciones criptográficas a otros paquetes como OpenSSH y navegadores web

## Comandos utiles

### Creando un certificado

 ``` bash
 openssl req -x509 -nodes -newkey rsa:2048 -days 5000 -sha256 -keyout localhost.pem -out localhost.pem
 ```

Desglosamos este comando por partes:

 `req`: crea y procesa principalmente solicitudes de certificado en formato PKCS
 `-x509`: Genere un certificado autofirmado en lugar de una solicitud de certificado
 `-nodes`: No cifre las llaves privadas
 `-newkey`: Crear nueva llave
 `rsa:2048`: Llave RSA de 2048 bits
 `-days`: Duración en dias del certificado
 `-sha256`: Resumen del mensaje con el que firmar la solicitud
 `-keyout`: El archivo en el que escribir la clave privada recién creada
 `-out`: El archivo de salida codificado en DER

### Cifrando y descifrando archivos

#### Cifrar

``` bash
openssl enc -aes-256-cbc -md sha512 -pbkdf2 -iter 100000 -salt -in $1 -out $2
```

Desglosamos este comando por partes:

`enc`: Sub comando para sifrado cimetrico
`-aes-256-cbc`: Cifrado AES adecuando para encriptar archivos locales
`-md`: El algoritmo de resumen que se utilizará al firmar o renunciar
`sha512`: El algoritmo usado para firmar sera sha512
`-pbkdf2`: Utilice la función de derivación de claves pbkdf2
`-iter`: Definir el numero de iteraciones
`-salt`: Incluir salt(es para que el hash sea unico)
`-in`: nombre o ruta del archivo a cifrar

#### Descifrar

``` bash
openssl enc -d -aes-256-cbc -md sha512 -pbkdf2 -iter 100000 -salt -in $1 -out $2
```

`-d`: Descifrar


nota: si deseas conocer mas a cerca de las banderas y subcomandos de openssl
[click a qui](https://man.openbsd.org/openssl.1)
