# CRUD

## CREATE

| command | Descipcion |
|--|--|
| mkdir somedir | crear un nuevo directorio |
| mkdir somedir-{1, 2, 3} | crear un un grupo de directorios |
| mkdir -P somedir/subdir/subdir | crear una estructura de directorios sin errores |
| touch somefile.txt | crear o tocar un archivo |
| cat > somefile.txt | crear un archivo |
| touch somefile.txt somefile-2.txt | crear o tocar multiples archivos |
| echo "some line" > somefile.txt | crear un archivo con una linea |
| cat << EOF > somefile.txt
    Some line
    Some other line
    EOF | crear un archivo com multiples lineas |

## READ

| command | Descipcion |
|--|--|
| cat somefile | ver el contenido de un archivo |
| tac somefile | ver el contenido de un archivo en orden inverso |
| cat somefile | cut -d '|' -f 2 | ver el contenido de un archivo que este despues de '|' |
| nl somefile | ver el contenido de un archivo con numero de linea |
| sed G somefile | ver el contenido de un archivo con doble espacios |
| sed '/jetbrains/q' somefile.txt | imprima hasta que se cumpla una determina |
| sed -n '/string1/p' somefile.txt | imprima solo las lineas que cumplen con la condicion |
| more somefile | paginacion del contenido a la salida standard |
| less somefile | ver el contenido de un archivo de forma interactiva |
| head somefile | ver las lineas superiores de un archivo |
| tail somefile | ver las ultimas lineas de un archivo |
| tail -f somefile.log | ver las ultimas lineas de un archivo en tiempo real |
| xdg-open somefile | abrir un archivo con el programa predefinido |

#Search

| command | Descipcion |
|--|--|
| grep -r "search_input" | buscar patron de busqueda forma recursiva |
| grep -ri "search_input" | buscar patron de busqueda forma recursiva con ignorecase |
| grep -rx "search_input" | buscar patron de busqueda exacto forma recursiva |
| find / -name file1 | busque archivos y directorios denominados file1. Iniciar búsqueda en la raíz (/) |
| find / -user user1 | busque archivos y directorios propiedad del usuario1. Iniciar búsqueda en la raíz (/) |
| find /home/user1 -name "*.bin" | busque todos los archivos y directorios cuyos nombres terminen en '.bin'. Iniciar búsqueda desde '/home/user1'* |
| find /usr/bin -type f -atime +100 | encontrar todos los archivos en '/usr/bin' a los que se accedió por última vez hace más de 100 días |
| find /usr/bin -type f -mtime -10 | buscar todos los archivos en '/usr/bin' creados o modificados en los últimos 10 días |
| find / -name *.rpm -exec chmod 755 '{}' \; | busque todos los archivos y directorios con nombres que terminen en '.rpm' y cambie sus permisos |
| find / -xdev -name "*.rpm" | busque todos los archivos y directorios con nombres que terminen en '.rpm', ignorando los medios extraíbles como cdrom, disquete, etc. |
| locate "*.ps" | busque todos los archivos que contengan '.ps' en el nombre. Se recomienda ejecutar el comando 'updatedb' de antemano |
| whereis halt | muestra la ubicación de binarios, fuentes y manuales relacionados con el archivo 'halt' |
| which halt | muestra la ruta completa al archivo 'halt' |

## UPDATE

| command | Descipcion |
|--|--|
| echo "new line" >> somefile | agregar una nueva linea al archivo |
| vi somefile | editar un archivo con vi |
| sed -e 's/input/output/' my_file > new_file | buscar y reemplazar apartir de un patron de busqueda |
| grep -r "search_input" -l | xargs sed -i 's/search_input/replacement/g | buscar y reemplazar de forma recursiva |
| find / -name myconfigfile.txt -exec sed -i 's/search_input/replacement/g | busque todos los archivos nombre 'myconfigfile.txt' y busqu y reemplace |

## DELETE

| command | Descipcion |
|--|--|
| rmdir somedir | eliminar un directorio vacio |
| rm -r somedir | eliminar un directorio de forma recursiva |
| rm -rf somedir | eliminar un directorio de forma recursiva de forma forzada |
| rm somefile | eliminar un archivo |
| sed -e '1d' somefile | eliminar la linea 1 de un archivo |
